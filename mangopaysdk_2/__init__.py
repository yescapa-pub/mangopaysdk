from __future__ import unicode_literals
import warnings


# Python ignores DeprecationWarning[s] by default
warnings.simplefilter('always', DeprecationWarning)
