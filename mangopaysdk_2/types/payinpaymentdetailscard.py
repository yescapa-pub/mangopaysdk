from __future__ import unicode_literals
from mangopaysdk_2.types.payinpaymentdetails import PayInPaymentDetails


class PayInPaymentDetailsCard(PayInPaymentDetails):
    """Class represents Card type for mean of payment in PayIn entity."""

    def __init__(self):
        # CardType enum
        self.CardType = None
        self.StatementDescriptor = None
