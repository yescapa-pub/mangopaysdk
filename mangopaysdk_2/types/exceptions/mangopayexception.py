from __future__ import unicode_literals
class MangoPayException(Exception):
    """ Base class for all MangoPaySDK exceptions.
    (Currently only one defined: ResponseException)
    """
    pass
