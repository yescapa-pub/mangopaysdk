from __future__ import unicode_literals
from mangopaysdk_2.types.dto import Dto


class PayInTemplateURLOptions(Dto):
    """Class represents PayInTemplateURLOptions object."""

    def __init__(self):
        self.PAYLINE = None
