from __future__ import unicode_literals
from mangopaysdk_2.types.payinpaymentdetails import PayInPaymentDetails


class PayInPaymentDetailsDirectCard(PayInPaymentDetails):
    def __init__(self):
        # CardType enum
        self.CardType = None
        self.SecureModeReturnURL = None
