﻿from __future__ import unicode_literals
from mangopaysdk_2.types.payinpaymentdetails import PayInPaymentDetails

class PayInPaymentDetailsPayPal(PayInPaymentDetails):
    """Class represents PayPal type for mean of payment in PayIn entity."""
