from __future__ import unicode_literals
from mangopaysdk_2.types.payinpaymentdetails import PayInPaymentDetails

class PayInPaymentDetailsBankWire(PayInPaymentDetails):
    """Class represents BankWire type for mean of payment in PayIn entity."""

    def __init__(self):
        self.BankAccount = None
        self.WireReference = None
        self.DeclaredDebitedFunds = None
        self.DeclaredFees = None

    def GetSubObjects(self):
        return {'BankAccount': 'BankAccount'}
