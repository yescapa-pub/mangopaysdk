﻿from __future__ import unicode_literals
from mangopaysdk_2.types.payinpaymentdetails import PayInPaymentDetails


class PayInPaymentDetailsDirectDebit(PayInPaymentDetails):
    
    def __init__(self):
        self.DirectDebitType = None
        self.MandateId = None
