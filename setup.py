from __future__ import unicode_literals
from setuptools import setup

# to build - python setup.py sdist upload
setup(
    name='mangopaysdk_2',
    version='2.1.0',
    author='Mangopay (www.mangopay.com)',
    author_email='support@mangopay.com',
    packages=['mangopaysdk_2', 'mangopaysdk_2.entities', 'mangopaysdk_2.tools', 'mangopaysdk_2.tools.storages', 'mangopaysdk_2.types', 'mangopaysdk_2.types.exceptions'],
    description='MANGOPAY API',
    long_description=open('README.md').read(),
    install_requires=[
        "requests>=2.4.3",
        "requests-oauthlib>=0.4.0",
        "fasteners>=0.14.1"
    ],
    keywords="emoney api sdk mangopay"
)
