# These targets are not files
.PHONY: coverage test

test:
	python test_suite.py

coverage:
	coverage run --source=mangopaysdk_2 test_suite.py	
